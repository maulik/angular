<div ng-app="">
Write some text in textbox:
<input type="text" ng-model="EnterText" class="form-control" />

<h1 ng-show="EnterText">Hello {{ EnterText }}</h1>    
<h1 ng-hide="EnterText">Hide on fill textbox</h1> 
<h4>Uppercase: {{ EnterText | uppercase }}</h4>
<h4>Lowercase: {{ EnterText | lowercase }}</h4>
</div>