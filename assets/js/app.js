//Define an angular module for our app
var sampleApp = angular.module('sampleApp', []);
 
//Define Routing for app
//Uri /AddNewOrder -> template add_order.html and Controller AddOrderController
//Uri /ShowOrders -> template show_orders.html and Controller AddOrderController
sampleApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/AddNewOrder', {
        templateUrl: 'tamplate/AddOrder.php',
        controller: 'AddOrderController'
    }).
      when('/ShowOrders', {
        templateUrl: 'tamplate/showOrder.php',
        controller: 'ShowOrdersController'
      }).
      when('/ShowOrdersNew/:OrderId', {
        templateUrl: 'tamplate/showOrderNew.php',
        controller: 'ShowOrdersNewController'
      }).        
      otherwise({
        redirectTo: '/AddNewOrder'
      });
}]);
 
 
sampleApp.controller('AddOrderController', function($scope) {
    $scope.message = 'This is Add new order screen';
});
 
 
sampleApp.controller('ShowOrdersController', function($scope) {
    $scope.message = 'This is Show orders screen';
});

sampleApp.controller('ShowOrdersNewController', function($scope,$routeParams) {
    $scope.order_id = $routeParams.OrderId;
});