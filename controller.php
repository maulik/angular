<div class="form-horizontal" role="form" ng-app=""> 
<div class="form-group" ng-controller="ContactController">
    <label class="col-sm-1 control-label">Email</label>
    <div class="col-sm-1">
      <input type="text" class="form-control" placeholder="Email" ng-model="newcontact">
    </div>
    <div class="col-sm-1">
      <button type="submit" class="btn btn-default"  ng-click="add()">Add</button>
    </div>
    <div class="col-sm-10">
        <h2>Contacts</h2>
        <ul>
            <li ng-repeat="contact in contacts"> {{ contact }} </li>
        </ul>
    </div>
  </div>
     
</div>
<script type="text/javascript">
function ContactController($scope) {
    $scope.contacts = ["hi@email.com", "hello@email.com"];
    $scope.add=function(){
        $scope.contacts.push($scope.newcontact);
         $scope.newcontact='';
    }
   
}
</script>

