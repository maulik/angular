<?php
// Author:- Maulik B Kanani
class database
{

    var $_sql            = '';   
    /** @var Internal variable to hold the connector resource */
    var $_resource        = '';   
    /** @var Internal variable to hold the query result*/      
    var $_result        = '';     
        /** @var Internal variable to hold the query result*/
    var $_insertId      = '';


    function __construct($arg)
    {
        $host = $arg['hostname'];
        $user = $arg['username'];
        $pass = $arg['password'];
        $db = $arg['Database'];
        if ($this->_resource = @mysql_connect( $host, $user, $pass ))
        {
            mysql_select_db($db) or die('Cant select database'.mysql_error());
            //echo "in";
        }
        else
        {
            echo "Could not connect to the database!";
            exit;
        } 
        error_reporting(0);
    }
   
    /**
    * Execute the query
    * @return mixed A database resource if successful, FALSE if not.
    */
    function query($sql)
    {
        $_sql = $sql;       
        return $_result = mysql_query($_sql);               
    }
   
    /**
    * Execute the query for insert
    * @return auto increment id
    */
    function insert($table, $dbFields)
    {
       
        
        $field = array();
        $value = array();
       
        foreach ( $dbFields as $k => $v)
        {
              $v = addslashes(stripslashes($v));
               
            $field[] = $k;
            $value[] = $v;           
        }
       
        $f = implode('`,`',$field);
        $val = implode("','",$value);
       
        $insertSql = "INSERT INTO `$table` (`$f`) VALUES ('$val')";       
       
        $result = mysql_query($insertSql);                                
        $this->_insertId = mysql_insert_id();
        if(!mysql_error())
        {
            return $this->_insertId;
        }
        else {
            echo mysql_error();   
            exit;
        }
    }
   
    /**
    * Execute the query for update
    * @return true for success
    */
    function update($table, $dbFields, $where) {
      //  echo $table;
        //echo '<pre>'; print_r($dbFields);
        
         $updateSql = "UPDATE $table SET ";
        $i=0;
        foreach ( $dbFields as $k => $v) {
          
            //$v = addslashes(stripslashes($v));  
          
            if ($i==0){
                $updateSql .= " $k = '$v' ";               
            }
            else{
                $updateSql .= ", $k = '$v' ";
            }           
            $i++;
        }
       
         $updateSql .= " WHERE $where";
      
        $result = mysql_query($updateSql);
        echo mysql_error();
       if(mysql_error()){
           exit;
       }
        return true;
    }
   
    /**
    * Execute the query for sekect
    * @return array contains result
    */
    function select($vars = "*", $table, $where = "", $orderBy = "", $groupBy = "", $resultType = MYSQL_ASSOC ){
       
        if ($vars != "*"){
            if (is_array($vars)){
                $vars = implode(",",$vars);
            }
        }               
               
        $selectSql = "SELECT ".$vars." FROM ".$table." WHERE 1 ".$where." ".$groupBy." ".$orderBy;
       
    //        echo $selectSql;
       
        $resource = mysql_query($selectSql);
       
        $result = array();
       
        while($row = mysql_fetch_array($resource,$resultType)){
            $result[] = $row;
        }
        return $result;
    }
  
    
    function get_results($selectSql, $data_type='json', $resultType = MYSQL_ASSOC ){
       
        $resource = mysql_query($selectSql);
       
        $result = array();
       
        while($row = mysql_fetch_array($resource,$resultType)){
            $result[]= $row;
        }
        
        if($data_type=='json')
        {
            return json_decode(json_encode($result));
        }else{
            return $result;
        }
    }
    
    
    
    
     function get_row($selectSql, $resultType = MYSQL_ASSOC ){
       
        $resource = mysql_query($selectSql);
       echo mysql_error();
        $result = array();
       
        while($row = mysql_fetch_array($resource,$resultType)){
            $result = $row;
        }
        return json_decode(json_encode($result));
    }
    
    /**
    * Execute the query for delete
    * @return true
    */
    function delete($table, $where)
    {

        $deleteSql = "DELETE FROM $table WHERE $where ";
        //exit;
               
        $result = mysql_query($deleteSql);
       
        return true;
    }
    
    function getNextId($table)
    {
        $result = mysql_query("SHOW TABLE STATUS LIKE '".$table."'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;    
    }
    
    
    
    function getName($id,$idValue,$table,$name){
        error_reporting(0);
         $sqlSelect = "SELECT `".$name."`
                         FROM `".$table."`
                         WHERE `".$id."` = '".$idValue."'";       
    //    echo $sqlSelect;             

        $relSelect = mysql_query($sqlSelect);
        $nameValue = "";
        while ($row = mysql_fetch_array($relSelect)){       
            $nameValue = $row[$name];
        }

        if($nameValue)
            return $nameValue;
        else
            return '';
    }
    
    /**
    * Called for taking last insert id
    * @return last inserted id
    */
    function getInsertId(){
        return $this->_insertId;
    }
   
    /**
    * Execute the query for num of row count
    * @return number of rows for result
    */
    function numRows($sql){
        $_sql = $sql;
        $_result = mysql_query($_sql);
        $results = mysql_num_rows($_result);
        mysql_free_result($_result);
        return $results;
    }
   
    /**
    * Clode db connection
    */
    function dbClose(){
        mysql_close($this->_resource);
    }
   
    /**
    * fetch the mysql result resource
    * @return fetched array
    */
    function fetchArray($rs){
        return @mysql_fetch_array($rs);
    }
}       
?>