<?php

require_once 'include/conf.inc.php';
Global $mdb;
extract($_POST);
$action();

function fetch() {
    global $mdb;
    $results = $mdb->get_results("select * from contacts");
    $data = array();
    foreach ($results as $result):
        $data[$result->id] = array(
            'id' => $result->id,
            'name' => $result->name,
            'email' => $result->email,
            'phone' => $result->number,
        );
    endforeach;
    echo json_encode($data);
    exit;
}

function add() {
    global $mdb;
    extract($_POST);
    $data = json_decode($data);
    $ins = array(
        'name' => $data->name,
        'email' => $data->email,
        'number' => $data->phone,
    );
    $mdb->insert('contacts', $ins);
    exit;
}

function update(){
    global $mdb;
    extract($_POST);
    $data = json_decode($data);
    $update=array(
        'name' => $data->name,
        'email' => $data->email,
        'number' => $data->phone,
    );    
    $where="id=$data->id";
    $mdb->update('contacts',$update,$where);
}
function delete() {
    global $mdb;
    extract($_POST);
    $where = " id=$id";
    $mdb->delete('contacts', $where);
}

?>
