<div  ng-app="">
<div ng-controller="AsiaCotroller">
    {{ child }} is in {{ parent }}
     
    <div ng-controller="IndiaController">
        {{ child }} is in {{ parent }}
 
        <div ng-controller="GujaratController">
             {{ child }} is in {{ parent }}
 
        </div>
    </div>
</div>

<script type="text/javascript">
    function AsiaCotroller($scope){
        $scope.child= 'Asia';
        $scope.parent= 'Earth';
    } 
    function IndiaController($scope){
        $scope.child= 'India';
        $scope.parent= 'Asia';
    } 
    function GujaratController($scope){
        $scope.child= 'Gujarat';
        $scope.parent= 'India';
    } 
</script>


<div ng-controller="BMWController">
     
    My name is {{ name }} and I am a {{ type }}
 
    <button ng-click="clickme()">Click Me</button> 
 
</div>
     
 <!-- Inheriteance--> 
<script>
function CarController($scope) {
  
    $scope.name = 'Car';
    $scope.type = 'Car';
  
    $scope.clickme = function() {
        alert('This is parent controller "CarController" calling '+$scope.name);
    }
 
}
  
function BMWController($scope, $injector) {
    
    $injector.invoke(CarController, this, {$scope: $scope});
    $scope.name = 'BMW';
    
  
}
</script>
</div>