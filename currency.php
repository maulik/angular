 <script>
    function Ctrl($scope) {
      $scope.amount = 1234.56;
    }
  </script>
 <div ng-controller="Ctrl" ng-app="">
    <input type="number" class="form-control"   ng-model="amount"> <br>
    default currency symbol ($): <span id="currency-default">{{amount | currency}}</span><br>
    custom currency identifier (USD$): <span>{{amount | currency:"INR"}}</span>
  </div>
