<script src="assets/js/app.js"></script>
<div ng-app="sampleApp">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="nav">
                    <li><a href="#AddNewOrder"> Add New Order </a></li>
                    <li><a href="#ShowOrders"> Show Order </a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <div ng-view></div>
            </div>
        </div>
    </div>

</div>


<div class="row">
    <div class="col-md-9">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th><th>Order No.</th><th>Details</th><th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td><td>1234</td><td>15" Samsung Laptop</td>
                    <td><a href="#ShowOrdersNew/1234">show details</a></td>
                </tr>
                <tr>
                    <td>2</td><td>5412</td><td>2TB Seagate Hard drive</td>
                    <td><a href="#ShowOrdersNew/5412">show details</a></td>
                </tr>
                <tr>
                    <td>3</td><td>9874</td><td>D-link router</td>
                    <td><a href="#ShowOrdersNew/9874">show details</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>



