<script>
    function commentsController($scope, $http)
    {

        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        $http.get("demodb.php?action=getComments").success(function(data)
        {
            $scope.comments = data;
        });

        $scope.addComment = function(comment) {
// Validate the comment is not an empty and undefined
            if ("undefined" != comment.msg) {
// Angular AJAX call
                $http({
                    method: "POST",
                    url: "demodb.php",
                    data: "action=add&msg=" + comment.msg
                }).success(function(data) {
// Add the data into DOM for future use
                    $scope.comments.unshift(data);
                });
                $scope.comment = "";
            }
        }

// index : index of global DOM
        $scope.deleteComment = function(index) {
// Angular AJAX call
            $http({
                method: "GET",
                url: "demodb.php?action=delete&id=" + $scope.comments[index].id,
            }).success(function(data) {
// Removing Data from Global DOM
                $scope.comments.splice(index, 1);
            });
        }
    }
</script>

<style type="text/css">
    * { padding:0px; margin:0px; }
    body{font-family:arial}
    textarea{border:solid 1px #333;width:520px;height:30px;font-family:arial;padding:5px}
    .main{margin:0 auto;width:600px; text-align:left;}
    .updates
    {
        padding:20px 10px 20px 10px ;
        border-bottom:dashed 1px #999;
        background-color:#f2f2f2;
    }
    .button
    {
        padding:10px;
        float:right;
        background-color:#006699;
        color:#fff;
        font-weight:bold;
        text-decoration:none;
    }
    .updates a
    {
        color:#cc0000;
        font-size:12px;
    }
</style>
<div ng-app id="ng-app" class="main">
    <div ng-controller="commentsController">

        <!-- Update Box -->
        <textarea name="submitComment" ng-model="comment.msg" placeholder="What are you thinking?"></textarea>
        <a href="javascript:void(0);" class="button" ng-click="addComment(comment)">POST</a>

        <!-- Comments -->
        <div ng-repeat="comment in comments">
            <div class="updates">
                <a href="javascript:void(0);" ng-click="deleteComment($index);">Delete</a>
                {{comment.msg}}
            </div>
        </div>

    </div>
</div>