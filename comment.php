<?php
require_once 'include/conf.inc.php';
class comments{
    var $con;

    public function getComments() {
        $qry = mysql_query("SELECT comment_id, comments, timestamp FROM comments ORDER BY timestamp DESC");
        $data = array();
        while ($rows = mysql_fetch_array($qry)) {
            $data[] = array("id" => $rows['comment_id'],
                "msg" => $rows['comments'],
                "timestamp" => $rows['timestamp']);
        }
        return json_encode($data);
    }

    public function addComment($post) {
        $comments = mysql_escape_string($post['msg']);
        $time = time();
        $id = 0;
        $qry = mysql_query("INSERT INTO comments(comments,timestamp)VALUES('{$comments}','{$time}')") or die(mysql_error());
        $id = mysql_insert_id();
        return json_encode(array("id" => $id,
            "msg" => stripslashes($comments),
            "timestamp" => $time));
    }

    public function deleteComment($id) {
        (int) $id = mysql_escape_string($id);
        $del = mysql_query("DELETE FROM comments WHERE comment_id = " . $id);
        if ($del)
            return true;
        return false;
    }

}

?>