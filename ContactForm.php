<div ng-app="contact" ng-controller="ContactController" class="ng-scope"  ng-app="">
    <form class="form" role="form">
        <div class="form-group">
            <div class="col-sm-10">  
                <label for="Name">Name</label>
            </div> 
            <div class="col-sm-3">
                <input type="text" class="form-control" id="Name" placeholder="Enter Name" ng-model="newcontact.name">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10"> 
                <label for="email">Email</label>
            </div> 
            <div class="col-sm-3">
                <input type="email" class="form-control" id="email" placeholder="Enter Email" ng-model="newcontact.email">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10"> 
                <label for="number">Number</label>
            </div> 
            <div class="col-sm-3">
                <input type="text" class="form-control" id="number" placeholder="Enter Number" ng-model="newcontact.phone">
            </div>
        </div>  
        <input type="hidden" ng-model="newcontact.id" />
        <div class="col-sm-10"  style="margin: 8px 0px 12px;"> 
            <button type="submit" class="btn btn-default" ng-click="add(newcontact.id);">Add</button>
        </div>    
    </form>
    <br>
    <div style="clear: both;"></div>
    <!--<div class="form-group">
        <div class="col-sm-1"> 
            <label for="number">Search</label>
        </div> 
        <div class="col-sm-3">
            <input type="text" class="form-control" id="number" placeholder="Enter Number" ng-model="search">
        </div>
    </div>-->
    
    <div class="input-group">
        <input class="form-control" ng-model="searchText" placeholder="Search" type="search"/> <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
        </span>
    </div>
    
    <div style="clear: both; margin: 5px 4px 25px 0px;"></div>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Name </th>
                <th>Email</th>
                <th>Phone </th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="contact in contacts | filter:searchText" class="ng-scope">
                <td class="ng-binding">{{ contact.name }} </td>
                <td class="ng-binding">{{ contact.email }}</td>
                <td class="ng-binding">{{ contact.phone }}</td>
                <td>
                    <a href="javascript:void(0)" ng-click="edit(contact.id)">edit</a> |
                    <a href="javascript:void(0)" ng-click="delete(contact.id)">delete</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    var contact = angular.module('contact', []);
    contact.controller('ContactController', function($scope, $http) {
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        $http({
            method: "POST",
            url: "contacts.php",
            data: "action=fetch"
        }).success(function(data) {
            $scope.contacts = data
        });



        $scope.add = function(id) {
            if ($scope.newcontact.id == null) {
                $http({
                    method: "POST",
                    url: "contacts.php",
                    data: "action=add&data=" + JSON.stringify($scope.newcontact)
                }).success(function(data) {
                    $scope.contacts.push($scope.newcontact);
                    $scope.newcontact = {};
                });

            } else {

                $http({
                    method: 'POST',
                    url: 'contacts.php',
                    data: 'action=update&data=' + JSON.stringify($scope.newcontact),
                }).success(function(data) {
                    $scope.contacts[id] = angular.copy($scope.newcontact);
                    $scope.newcontact = {};
                });
            }
        }

        $scope.edit = function(id) {
            $scope.newcontact = angular.copy($scope.contacts[id]);
        }

        $scope.delete = function(id) {
            $http({
                method: "POST",
                url: "contacts.php",
                data: "action=delete&id=" + id
            }).success(function(data) {
                delete $scope.contacts[id];
            });


        }
        
    


    })
</script>